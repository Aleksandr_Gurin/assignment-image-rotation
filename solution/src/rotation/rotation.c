#include "rotation.h"

bool rotate(image *img1, image *img2){
    if(!new_image(img2, img1->width, img1->height))
        return false;
    
    for(uint32_t row=0; row<img1->height; row++){
        for(uint32_t col=0; col<img1->width; col++){
            color c = image_color_at(*img1, row, col);
            image_set_color(*img2, col, img1->height-1-row, c);
        }
    }
    
    return true;
}
