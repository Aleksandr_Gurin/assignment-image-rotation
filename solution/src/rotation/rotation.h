#ifndef ROTATION
#define ROTATION

#include "../image/image.h"

bool rotate(image *img1, image *img2);

#endif
