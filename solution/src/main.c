#include <stdio.h>

#include "image/image.h"
#include "image_io/image_io.h"
#include "rotation/rotation.h"

void usage(char *prog_path){
    fprintf(stderr, "Usage: %s input.bmp output.bmp\n", prog_path);
}

int main( int argc, char** argv ) {

    if(argc < 3){
        usage(argv[0]);
        return 1;
    }
    
    FILE *input = fopen(argv[1], "r");
    if(!input){
        fprintf(stderr, "Cannot open %s\n", argv[1]);
        return 1;
    }
    FILE *output = fopen(argv[2], "w");
    if(!output){
        fprintf(stderr, "Cannot open %s\n", argv[2]);
        fclose(input);
        return 1;
    }

    image img, img2;
    if(read_bmp(input, &img) != READ_OK){
        fprintf(stderr, "Cannot read BMP\n");
        fclose(input);
        fclose(output);
        delete_image(img);
        return 1;
    }

    if(!rotate(&img, &img2)){
        fprintf(stderr, "Cannot rotate\n");
        fclose(input);
        fclose(output);
        delete_image(img);
        delete_image(img2);
        return 1;
    }

    if(write_bmp(output, &img2) != WRITE_OK){
        fprintf(stderr, "Cannot write BMP\n");
        fclose(input);
        fclose(output);
        delete_image(img);
        delete_image(img2);
        return 1;
    }

    fclose(input);
    fclose(output);
    delete_image(img);
    delete_image(img2);

    return 0;
}
