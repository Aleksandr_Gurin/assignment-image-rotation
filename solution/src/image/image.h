#ifndef IMAGE
#define IMAGE

#include <stdbool.h>
#include <stdint.h>

typedef struct color { uint8_t b, g, r; } color;

typedef struct image {
  uint64_t width, height;
  struct color* data;
} image;

bool new_image(image *img, uint32_t h, uint32_t w);
void delete_image(image img);

color image_color_at(image img, uint32_t x, uint32_t y);
void image_set_color(image img, uint32_t x, uint32_t y, color c);

#endif

