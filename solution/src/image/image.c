#include "image.h"
#include <stdlib.h>

bool new_image(image *img, uint32_t h, uint32_t w){
    img->width = w;
    img->height = h;
    img->data = malloc(sizeof(color) * w * h);
    if(img->data == NULL)
        return false;
    return true;
}

void delete_image(image img){
    free(img.data);
}

color image_color_at(image img, uint32_t x, uint32_t y){
    return img.data[img.width * x + y];
}

void image_set_color(image img, uint32_t x, uint32_t y, color c){
    img.data[img.width * x + y] = c;
}
