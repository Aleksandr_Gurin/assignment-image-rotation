#ifndef IMAGE_IO
#define IMAGE_IO

#include "../image/image.h"
#include <stdio.h>

typedef enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_IMAGE_TOO_BIG,
    READ_ERROR
} read_status;

typedef enum write_status  {
    WRITE_OK,
    WRITE_ERROR
} write_status;


read_status read_bmp(FILE *f, image *img);
write_status write_bmp(FILE *f, image *img);

#endif
