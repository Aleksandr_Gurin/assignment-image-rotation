#include "image_io.h"


#include  <stdint.h>
typedef struct bmp_header{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed)) bmp_header;

static const bmp_header bmp_header_pattern = (bmp_header){
    .bfType = 19778,
    .bfReserved = 0,
    .biPlanes = 1,
    .biBitCount = 24,
    .bOffBits = sizeof(bmp_header),
    .biCompression = 0,
    .biSize = 40,
    .biXPelsPerMeter = 0,
    .biYPelsPerMeter = 0,
    .biClrUsed = 0,
    .biClrImportant = 0
};


read_status read_bmp(FILE *f, image *img){
    bmp_header header;
    if(fread(&header, sizeof(bmp_header), 1, f) != 1){
        return READ_ERROR;
    }

    if(header.bfType != bmp_header_pattern.bfType){
        return READ_INVALID_HEADER;
    }

    if(!new_image(img, header.biHeight, header.biWidth)){
        return READ_IMAGE_TOO_BIG;
    }

    for(uint32_t row = 0; row < img->height; row++){
        if(fread(img->data + row * img->width, sizeof(color), img->width, f) != img->width){
            return READ_ERROR;
        }

        unsigned int padding = 0;
        unsigned int width_bytes = sizeof(color) * img->width;
        if(width_bytes % 4 > 0)
            padding = 4 - (width_bytes % 4);
        
        if(fseek(f, padding, SEEK_CUR) != 0)
            return READ_ERROR;
    }    

    return READ_OK;
}


write_status write_bmp(FILE *f, image *img){
    unsigned int padding = 0;
    unsigned int width_bytes = sizeof(color) * img->width;
    if(width_bytes % 4 > 0)
        padding = 4 - (width_bytes % 4);
        
    bmp_header header = bmp_header_pattern;
    header.biSizeImage = (sizeof(color) * img->width + padding) * img->height;
    header.bfileSize = sizeof(bmp_header) + header.biSizeImage;
    header.biWidth = img->width;
    header.biHeight = img->height;

    if(fwrite(&header, sizeof(bmp_header), 1, f) != 1)
        return WRITE_ERROR;
    
    for(uint32_t row = 0; row < img->height; row++){
        if(fwrite(img->data + img->width * row, sizeof(color), img->width, f) != img->width)
            return WRITE_ERROR;
        
        uint32_t zeros = 0;
        if(fwrite(&zeros, 1, padding, f) != padding)
            return WRITE_ERROR;
    }
    return WRITE_OK;
}
